# SUMMARY



* [0. 有挑战, 更有意思](ch0/README.md)
* [1. 看起来像json, 叫起来像...](ch1/README.md)
* [2. 组队, 欢乐的抄作业](ch2/README.md)
  * [2.1 我们是有主页的小组](ch2/section2.1.md)
* [3. 一半是火焰, 一半泼了水](ch3/README.md)
  * [3.1 最远的路就是绕路](ch3/section1.1.md)
  * [3.2 Json趟坑笔记](ch3/section1.2.md)
* [4. 团队，协作](ch4/README.md)
  * [4.1 CI是一部侦探小说](ch4/webhooks.md)
  * [4.2 怎么把大象放进冰箱？](ch4/CI2.md)


