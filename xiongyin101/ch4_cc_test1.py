import json, requests
import pandas as pd
import os
import configparser
import numpy as np
from pandas import DataFrame
import matplotlib.pyplot as plt



config=configparser.ConfigParser()
config.read("config.ini")
# print(config.sections()) 验证


PROJECT_ID = config['tasks']['project_id']
PRIVATE_TOKEN = config['tasks']['private_token']
BASE_URL = config['tasks']['base_url']

#命名函数
def jsonlist2file(jsonlist, output_file):
    print(f"Does {output_file} exist?{os.path.exists(output_file)}")
    print("CTRL-C to abort.")
    input()
    
    out_file = open(output_file, 'w')
    out_file.write(json.dumps(jsonlist, sort_keys=True, indent=4))
    print(f"{output_file}, done")
    out_file.close()


def get_url_pages(session, url, headers, params):
    #获得所有页，返回json列表
    params['page'] = '1'
    r = session.get(url, headers = headers, params=params)
    r_list = r.json()

# print (json.dumps(r.json(), sort_keys=True, indent=4))
  
    

    get_times = int(r.headers['X-Total']) // int(params['per_page']) 

# print(f"页数 : {get_times + 1}")
    
    for i in range(get_times):
        params['page'] = str(i + 2)
        r = (session.get(url, headers = headers, params=params))
        r_list.extend(r.json()) 
        
    return(r_list)


# ### 获取commits
# curl --header "PRIVATE-TOKEN:-43KkjBS1U95zin8pfW7" "https://gitlab.com/api/v4/projects/17730311/repository/commits"

session = requests.session()
#避免报错
headers = requests.utils.default_headers()
headers['User-Agent'] = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'
headers['PRIVATE-TOKEN'] = PRIVATE_TOKEN


"""
requests.utils
~~~~~~~~~~~~~~

This module provides utility functions that are used within Requests
that are also useful for external consumption.
"""
# 这块目前还是不很理解，为什么要用requests.utils

#获取所有的commits,放入list
params = {
    'per_page' : '100',
    'all' : 'True'
}
url = f'{BASE_URL}api/v4/projects/{PROJECT_ID}/repository/commits'
commits_list = get_url_pages(session, url, headers, params)

commits_df = pd.DataFrame(commits_list)
commits_df.head()
# 从list中，获取 commits的部分值

detail_list = []
comments_list = []
for i in commits_list:
    print('#',end='') #进度条
    #print(i['id'])
    # 获取 single commit
    url = f"{BASE_URL}api/v4/projects/{PROJECT_ID}/repository/commits/{i['id']}"
    commit = session.get(url, headers = headers)
    # commit.json() 是一个字典
    #print(commit.json()['author_name'])
   
    

    # part2. 获取 commit discussions
    url = f"{BASE_URL}api/v4/projects/{PROJECT_ID}/repository/commits/{i['id']}/discussions"
    params = {
    'per_page' : '10',
    'all' : 'True'
    }
    discussions = get_url_pages(session, url, headers, params)
    
    # if .json() != []: #大部分 commit 没有discussions
    for j in discussions:
        # print(k) 注意其中 notes 本身是一个list
        comments_count = len(j['notes']) 
        for k in j['notes']:
            if k['type'] == "DiffNote":
                target_line = k['position']['old_line']
            else:
                target_line = None
 
        # 编写一个 字典    ？？？？这里的字典编写方式为什么和commits的不一样？
        comment_dict = {
            'id' : k['id'],
            'author_name' : k['author']['name'],
            'comments_count' : comments_count,
        }
        comments_list.append(comment_dict)
              
#以上都是抄ch3作业，以下为ch4部分

cc_data = pd.DataFrame(comments_list) 
# 命名要分组的数据库

cc_data = cc_data['author_name'].value_counts().reset_index()
cc_data.columns=['author_name', 'count']
#获取每个学员commit comments数量，然后把这个转化成一个dataframe
#打印前五个

cc = cc_data.head(5)

print(cc)


#图形显示

Data = cc_data
cc_ranking = DataFrame(Data, columns = ['author_name','count'])
cc_ranking.plot(x = 'author_name', y = 'count', kind = 'barh', color='o')
plt.show()