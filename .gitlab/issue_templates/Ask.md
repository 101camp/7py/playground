> 日常提问模板

## 环境
> 陈述所在操作系统/版本, Python 版本, 相关依赖模块/...

- 系统: 类似 macOS 10.12.6 (16G2136)
- Py版本: 类似 Python 3.7.3 (default, Mar 27 2019, 16:54:48)
    + [Clang 4.0.1 (tags/RELEASE_401/final)] :: Anaconda, Inc. on darwin
- 编辑器: 类似 Sublime text 3.2.2

## 背景
> 问题产生的背景任务/目标/...

## 现象
> [5W1H](https://wiki.woodpecker.org.cn/moin/5W1H)

- When? 精确发生时间点
    + 不止是时间, 还包含事件序列
    + 比如:
        * 先准备了什么
        * 输入了什么
        * 触发了当前问题
- Where? 在什么场景中,什么数据上下文?...
    + 不止是输入, 还包含所有运行时应该明确的周围环境
    + 网络: wifi 2g? 4G 手机? 有线?
    + 什么终端软件中?版本? 什么浏览器中?版本? ...
- What? 想完成什么目标? 操作序列, 指令, 终端输出?...
    + 问题触发时, 原本目的是什么?
    + 具体想达到什么目标?
    + 输入什么, 期望获得什么?
- Why? 如何理解的? 
    + 当前自己的理解, 分析...
- How? 进行了什么尝试?
    + 已经搜索了什么关键词, 每次都看了什么文章?
    + 记录形式类似:
        * Bing搜索: python 多版本
        * 参考:
            - [Python多版本管理 - 简书](https://www.jianshu.com/p/37576a6de65b)
            - [windows配置Python多版本共存 - 尹正杰 - 博客园](https://www.cnblogs.com/yinzhengjie/p/9106558.html)
            - [多个版本python_多个python版本_多版本python - 云+社区 - 腾讯云](https://cloud.tencent.com/developer/information/%E5%A4%9A%E4%B8%AA%E7%89%88%E6%9C%ACpython)
            - ...



## 分析
> 自己的理解, 探索, 没实验通的关键问题

针对当前问题和探索, 自己的假设:

- 假设0:
    + 条件0: ...
    + 实验0: ...
    + 结果0: ...
    + 暂时结论0: ...
- 假设1:
    + 条件1: ...
    + ...



## refer
永远的: [提问的智慧](https://github.com/DebugUself/How-To-Ask-Questions-The-Smart-Way/blob/master/README-zh_CN.md)


- [问题讨论与解决问题的七个回合法_黄冬_新浪博客](http://blog.sina.com.cn/s/blog_5595d51401000022.html)


## logging:
> 用倒序日期排列来从旧到新记要关键变化


- 200315 ZQ 修订 提问的智慧外链
- 191211 ZQ 增补细节和问题讨论的回合
- 190724 ZQ 增强标题结构
- 190714 ZQ 部署到 tasks 仓库
- 190704 ZQ init.
