# ch3. 趟坑手册，最远的路就是绕路

> 最远的路就是绕路

1. 为了取得request的文件头，绕了个远路：requests.head

   Google : requests.head python 3 ~  [Python请求头方法](https://www.w3schools.com/python/ref_requests_head.asp)  

2. Json.dump 中文变成了”\u5176\u5b83”的形式

   [python json dump 输出中文 - 李涛的博客 | Tao's Blog](http://litaotju.github.io/python/2016/06/28/python-json-dump-utf/) `让dump输出真正的中文`

3. gitlab不能上传图片？

   [Image uploading does not work in comments (gitlab.com) (#38721) ](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/38721)	`方法简单到意想不到`

4. Connection aborted.', OSError("(104, 'ECONNRESET')？专治各种链接中断

   [Requests ConnectionError: ('Connection aborted.', OSError("(104, 'ECONNRESET')"](https://stackoverflow.com/questions/43165341/python3-requests-connectionerror-connection-aborted-oserror104-econnr) `不明觉厉,似乎解决`

   [session用法 — Requests 2.18.1 文档](https://requests.readthedocs.io/zh_CN/latest/user/advanced.html) `这个高档了，有用处吗？`

5. 两个json怎么和并，能不能用append?

   [Python List extend()方法 | 菜鸟教程](https://www.runoob.com/python/att-list-extend.html) `从zhouys代码里学的`

6. Jupiter notebook怎么取当前目录？

   [[Python] 获取当前路径 - 简书(不起作用)](https://www.jianshu.com/p/d168e8ccdd6d) `无效`

   [file does not exist in Jupyter Notebook](https://stackoverflow.com/questions/39125532/file-does-not-exist-in-jupyter-notebook) `无效`

7. No connection adapters ？

   [Python Requests - No connection adapters - Stack Overflow](https://stackoverflow.com/questions/15115328/python-requests-no-connection-adapters) `最后发现写成htts:\\，心痛十五分钟`

