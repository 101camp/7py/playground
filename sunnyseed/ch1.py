# 读取文件，参考e23
def get_list(txt):
	line = txt.readline()
	if line:
		# stripping of the trailing \n on the line string
		line = line.strip()
		# 取得orphan名字
		# str.split(str="",num=string.count(str))[n]
		orphan_list.append(line.split("/")[-1])
		#print(f"{orphan_list}")
		# calling main again inside main
		return get_list(txt)
		
"""
* master
  remotes/origin/HEAD -> origin/master
  remotes/origin/byakunine
  remotes/origin/jiaqili
  remotes/origin/kittypang
  remotes/origin/liyuxin
  remotes/origin/magico101
  remotes/origin/master
  remotes/origin/py101
  remotes/origin/sunnyseed
  remotes/origin/swufrances
  remotes/origin/tonony
  remotes/origin/woshizezi
  remotes/origin/xiongyin101
  remotes/origin/ysawybb
  remotes/origin/zhouys101
"""

# 执行命令获得orphan分支，参考ch0
import os
filename = "userlist.txt"
os.system(f"git branch -a > {filename}")
print(f"1.获得当前orphan列表，存放至{filename}")

#读取文件
txt = open(filename)
txt.seek(0)
# print(txt.read())
orphan_list = []
get_list(txt)
# 从第三个开始打印
print("2.目前orphan列表")
for i in range(2,len(orphan_list)):
	print(f"({i}) {orphan_list[i]}")

txt.close()

