import json, requests
import pandas as pd
import os
import configparser
import numpy as np


config=configparser.ConfigParser()
config.read("config.ini")
# print(config.sections()) 验证


PROJECT_ID = config['tasks']['project_id']
PRIVATE_TOKEN = config['tasks']['private_token']
BASE_URL = config['tasks']['base_url']

#命名函数
def jsonlist2file(jsonlist, output_file):
    print(f"Does {output_file} exist?{os.path.exists(output_file)}")
    print("CTRL-C to abort.")
    input()
    
    out_file = open(output_file, 'w')
    out_file.write(json.dumps(jsonlist, sort_keys=True, indent=4))
    print(f"{output_file}, done")
    out_file.close()


def get_url_pages(session, url, headers, params):
    #获得所有页，返回json列表
    params['page'] = '1'
    r = session.get(url, headers = headers, params=params)
    r_list = r.json()

# print (json.dumps(r.json(), sort_keys=True, indent=4))
  
    

    get_times = int(r.headers['X-Total']) // int(params['per_page']) 

# print(f"页数 : {get_times + 1}")
    
    for i in range(get_times):
        params['page'] = str(i + 2)
        r = (session.get(url, headers = headers, params=params))
        r_list.extend(r.json()) 
        
    return(r_list)


# ### 获取commits
# curl --header "PRIVATE-TOKEN:-43KkjBS1U95zin8pfW7" "https://gitlab.com/api/v4/projects/17730311/repository/commits"

session = requests.session()
#避免报错
headers = requests.utils.default_headers()
headers['User-Agent'] = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'
headers['PRIVATE-TOKEN'] = PRIVATE_TOKEN


"""
requests.utils
~~~~~~~~~~~~~~

This module provides utility functions that are used within Requests
that are also useful for external consumption.
"""
# 这块目前还是不很理解，为什么要用requests.utils

#获取所有的commits,放入list
params = {
    'per_page' : '100',
    'all' : 'True'
}
url = f'{BASE_URL}api/v4/projects/{PROJECT_ID}/repository/commits'
commits_list = get_url_pages(session, url, headers, params)

commits_df = pd.DataFrame(commits_list)
commits_df.head()
# 从list中，获取 commits的部分值
detail_list = []
for i in commits_list:
    print('#',end='') #进度条
    #print(i['id'])
    # 获取 single commit
    url = f"{BASE_URL}api/v4/projects/{PROJECT_ID}/repository/commits/{i['id']}"
    commit = session.get(url, headers = headers)
    # commit.json() 是一个字典
    #print(commit.json()['author_name'])
          
    # 编写一个 字典
    commit_dict = {
        'author_name' : commit.json()['author_name'],
        'committed_date' : commit.json()['committed_date'],
    }
    detail_list.append(commit_dict)
    # print(f": {len(detail_list)}")
 
#以上都是抄ch3作业，以下为ch4部分

ci_data = pd.DataFrame(detail_list) 
    # 命名要分组的数据库

ci_data = ci_data['author_name'].value_counts().reset_index()
ci_data.columns=['author_name', 'count']
 #获取每个学员commit数量，然后把这个转化成一个dataframe

print(ci_data)   
print(type(ci_data))   
# <class 'pandas.core.series.Series'>

#这回输出的数据对啦！但是。。。。。没有用到group by 呢。
#现在是不是直接打印0-5行就行了嘛

ci = ci_data.head(5)

print(ci)
