# ch0-预热: 有挑战才有意思

> 蟒营™学习笔记 week1 200415
>
> 关于预热任务

## 背景

> 当前自己主要学习输出,一般周1创建, 每天增补

- 私人分支: 链接->[仓库](https://gitlab.com/101camp/7py/playground/-/tree/sunnyseed)
- 使用手册: 链接->[维基](https://gitlab.com/101camp/7py/tasks/-/wikis/Usage/sunnyseed)
- 时间帐单: 链接-> #1 对应线索起点


## 进展

- 200415 2h 通读ch0,加入SLACK，安装GIT; Todo: 预热任务
- 200416 4h 安装python3.7，安装git,完成预热任务的第一部分（clone, push); Todo:继续预热任务 比想象中复杂啊
- 200417 1h orphan clone, 失败; Todo：明天继续
- 200418 6h orphan 成功，ch0 预热任务部分完成 Todo：预习任务
- 200419 3h ch0预习任务LPTHW完成e01-e05 TODO e06-e10

- 成果0 -> 安装python 3.7
- 成果1 -> 安装git
- 成果2 -> clone orphan 分支
- 成果3 -> [lpthw e01-05](4062bae10e691f38f2dbe1945d2d3f6565d4d447)

## 安装 ANACONDA

参考：https://zhuanlan.zhihu.com/p/51137150

解决：安装完要重启终端

### 问题：git安装和基本使用

参考：

https://www.jianshu.com/p/7edb6b838a2e

https://gitlab.com/101camp/7py/tasks/-/wikis/How2/How2UseGit

解决：

1. 从AppStore安装Xcode（安装brew始终不成功，网络问题你懂的）
2. 创建ssh key、配置git 
3. 把ssh key 加入 github / gitlab
4. git clone git@gitlab.com:101camp/7py/playground.git
5. (touch ....md)
6. git status
7. git add .
8. git commit -m "First study note"
9. git push

### 问题：如何克隆一个纯净的ORPHAN分支

目的：用git branch -a 看没有clone别人的分支，这样就减少容量，避免串户

参考：

https://gitlab.com/101camp/7py/tasks/-/wikis/How2/How2UsageOrphanBranch

https://gitlab.com/101camp/7py/tasks/-/issues/14

解决：

第一天半夜看了issue，照做不成功，第二天30分钟搞定啦，

*关键是要一个干净的目录*

第一天看的文章不对，还漏了 get init

```python
(base) zhuym@zhuyimindeMac-mini orphan % pwd
/Users/zhuym/documents/101camp/orphan
(base) zhuym@zhuyimindeMac-mini orphan % git init
Initialized empty Git repository in /Users/zhuym/Documents/101camp/orphan/.git/
(base) zhuym@zhuyimindeMac-mini orphan % git remote add -t sunnyseed -f origin git@gitlab.com:101camp/7py/playground.git
...
 * [new branch]      sunnyseed  -> origin/sunnyseed
(base) zhuym@zhuyimindeMac-mini orphan % git checkout sunnyseed
Branch 'sunnyseed' set up to track remote branch 'sunnyseed' from 'origin'.
Switched to a new branch 'sunnyseed'
(base) zhuym@zhuyimindeMac-mini orphan % git branch -a
* sunnyseed
  remotes/origin/sunnyseed
(base) zhuym@zhuyimindeMac-mini orphan % ls  
LPTHW		README.md	logging
(base) zhuym@zhuyimindeMac-mini orphan % git pull
Enter passphrase for key '/Users/zhuym/.ssh/id_rsa': 
Already up to date.
```

### 问题：用git status检查当下目录状态

参考：

https://gitlab.com/101camp/7py/tasks/-/issues/14

解答：

1.干净的目录状态

```
$ git status
On branch master
Your branch is up-to-date with 'origin/master'.
nothing to commit, working directory clean
```

2.新增文件但未跟踪（Untracked）

```
$ echo 'My Project' > README
$ git status
On branch master
Your branch is up-to-date with 'origin/master'.
Untracked files:
  (use "git add <file>..." to include in what will be committed)
    README
nothing added to commit but untracked files present (use "git add" to track)
```

3.跟踪（add）新文件后暂存状态

```
$ git add README$ git status
On branch master
Your branch is up-to-date with 'origin/master'.
Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
    new file:   README
```

这时就可以执行commit命令进行提交，提交后进入git本地仓库，可以被push到远程仓库

### hello

先是在git网页找链接，后来突然想通了，用 git log 吧，就下面这样了

- playground -> playground@a1748c4b78722d5b0616d5f88cd3bd2208e3f147
- tasks -> tasks@d4630949fd8d84513c7925102ecb2c33fe349fa4
- tasks 私人本地工作复本 -> tasks@3eb1cb26117514559caed77a38c6b63fca27ddd6
- tasks/wikis -> wikis@179b41e0bc5be69155577bd58ff70999fd9c0746


## refer

- 过程中参考过的重要文章/图书/模块/代码/...
- 过程中和课程关系不大的issue

永远的: [提问的智慧](https://github.com/DebugUself/How-To-Ask-Questions-The-Smart-Way/blob/master/README-zh_CN.md)

### mac 2k屏字体太小

搜索：mac 放大字体

参考：https://www.jianshu.com/p/4ea389848679

https://github.com/xzhih/one-key-hidpi

解决：

macOS 2K 显示屏开启 HiDPI 解决字体小或模糊

需要关闭 打开 sip ：https://sspai.com/post/55066

### 安装homebrew 

>Failed to connect to raw.githubusercontent.com port 443: Connection refused

参考： [mac安装homebrew失败怎么办？ - 知乎](https://www.zhihu.com/question/35928898) 

https://zhuanlan.zhihu.com/p/111014448

解决：

还是要用google搜索啊，其他bing,baidu都是坑

```
/bin/zsh -c "$(curl -fsSL https://gitee.com/cunkai/HomebrewCN/raw/master/Homebrew.sh)"
```

**下面都不对**

1. 解决dns污染

在https://www.ipaddress.com/查询raw.githubusercontent.com的真实IP。

修改hosts

```
sudo vim /etc/hosts
```

添加如下内容：

199.232.68.133 raw.githubusercontent.com

2. 还是不行，挂v

> error: RPC failed; curl 56 LibreSSL SSL_read: SSL_ERROR_SYSCALL, errno 60

3. 再加140.82.114.3  github.com

### 卸载 MINICONDA（打算同时安装 tensorflow 环境）

搜索：mac uninstall miniconda

参考：https://www.quora.com/How-does-one-uninstall-miniconda

```
rm -r ~/miniconda/
```

解决：

```
(base) zhuym@zhuyimindeMac-mini ~ % which python
/Users/zhuym/opt/miniconda3/bin/python
(base) zhuym@zhuyimindeMac-mini ~ % cd /Users/zhuym/opt/
(base) zhuym@zhuyimindeMac-mini opt % rm -r miniconda3
```

### commit 引用

https://gitlab.com/101camp/7py/tasks/-/issues/30

| Reference type          | Raw reference                                                | Short link                |
| ----------------------- | ------------------------------------------------------------ | ------------------------- |
| Commit URL              | https://github.com/jlord/sheetsee.js/commit/a5c3785ed8d6a35868bc169f07e40e889087fd2e | a5c3785                   |
| SHA                     | a5c3785ed8d6a35868bc169f07e40e889087fd2e                     | a5c3785                   |
| User@SHA                | jlord@a5c3785ed8d6a35868bc169f07e40e889087fd2e               | jlord@a5c3785             |
| Username/Repository@SHA | User/Repository@SHA: jlord/sheetsee.js@a5c3785ed8d6a35868bc169f07e40e889087fd2e | jlord/sheetsee.js@a5c3785 |

