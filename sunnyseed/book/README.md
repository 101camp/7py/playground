# 乐豆派@sunnyseed

### 团队信息

- 中文名 乐豆派（乐高+豆瓣+python，谐音：快乐的与python战斗）

- 英文名 pythonfun

- 缩写 SJX

- 成员

  1. [sunnyseed](https://gitlab.com/sunnyseed) (队长)
  2. [xiongyin](https://gitlab.com/xiongyin101)
  3. [jiaqili](https://gitlab.com/jiaqili)

- 协力

  1. [协力分支: 乐豆派@sunnyseed](https://gitlab.com/101camp/7py/tasks/-/tree/sunnyseed)

  2. [团队链接: logging/Team/TeamSJX.md](https://gitlab.com/101camp/7py/tasks/-/blob/sunnyseed/logging/Team/TeamSJX.md) 

- 作品方向概述：初选课题是“极简版的乐高视频豆瓣”

  1. 抓取youtube或B站 乐高相关视频，例如非法搭建或乐高master

  2. 在网站输出，视频可评分，可评论

     

### sunnyseed私人链接 - 喜爱编程|愿意分享|数据展现｜图像识别

1. [私人 orphan 分支](https://gitlab.com/101camp/7py/tasks/-/tree/sunnyseed)
2. [私人 游乐场 实验目录](https://gitlab.com/101camp/7py/playground/-/tree/sunnyseed)
3. [私人 使用指南 wiki](https://gitlab.com/101camp/7py/tasks/-/wikis/Usage/sunnyseed)
4. [招募说明](https://gitlab.com/101camp/7py/tasks/-/issues/63)

- 镇楼

<img src="https://raw.githubusercontent.com/sunnyseed/gc4/master/images/mycat.jpeg" alt="my cat" style="zoom:50%;" />

### Sayeahooo

      The hard way is easier.
      &
      来吧，有坎一起过，有景共同赏。
     
                             --- py.101.camp / 7py / sunnyseed

