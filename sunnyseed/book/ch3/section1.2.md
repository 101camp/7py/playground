# ch3. Json趟坑笔记

> 蟒营™学习笔记

    [note] sunnyseed: 我趟过的Json坑

## 背景

> 我在json的格式上卡了挺久，看小伙伴也遇到障碍，记录下自己的理解。


## 问题

- Json是什么？
- 怎么读取Json中的值？
- 怎么制造一个Json？
- 把 Json 存放入文件
- 从文件读取 Json

### Json是什么？

>JSON建构于两种结构：

> “名称/值”对的集合（A collection of name/value pairs）= 字典（dictionary）

> 值的有序列表（An ordered list of values）= 列表（list）

```json
[
  {
    "id": 302,
    "body": "closed",
    "attachment": null,
    "author": {
      "id": 1,
      "username": "pipin",
      "email": "admin@example.com",
      "name": "Pip",
      "state": "active",
      "created_at": "2013-09-30T13:46:01Z"
    },
    "created_at": "2013-10-02T09:22:45Z",
    "updated_at": "2013-10-02T10:22:45Z",
    "system": true,
    "noteable_id": 377,
    "noteable_type": "Issue",
    "noteable_iid": 377,
    "resolvable": false,
    "confidential": false
  },
  {
    "id": 305,
    "body": "Text of the comment\r\n",
    "attachment": null,
    "author": {
      "id": 1,
      "username": "pipin",
      "email": "admin@example.com",
      "name": "Pip",
      "state": "active",
      "created_at": "2013-09-30T13:46:01Z"
    },
    "created_at": "2013-10-02T09:56:03Z",
    "updated_at": "2013-10-02T09:56:03Z",
    "system": true,
    "noteable_id": 121,
    "noteable_type": "Issue",
    "noteable_iid": 121,
    "resolvable": false,
    "confidential": true
  }
]
```

上面这个就是json，准确的说是 **格式化** 后的json。

放入[Online JSON Viewer](http://jsonviewer.stack.hu/) 检查一下

![](section1.2.assets/007S8ZIlgy1gem5bkqwqij308b0kpmyf.jpg)

- 从外形上看，最外层是一个列表，每个列表项是一个字典，部分字典项目本身，又是嵌套的字典（套娃即视感）

#### 怎么读取json中的值？

```python
# 上面第一层是list,读取list里的每个 item
for i in list_issues:     # 这时候 i 就是第二层的一个 item
    id = i["id"],  # 通过名称 获得 值
    author_id = i["author"]["id"] # 直接获得第二层 字典值

```

### 怎么制造一个Json？

```python
#把上面这句改一下, 定义一个列表
new_issues = []
for i in list_issues:
    # 把数据放入一个list 套 dict 的结构
    dict_issue = {
        "id" : i["id"],
        "iid" : i["iid"],
        "author_id" : i["author"]["id"],
				...
    }
    new_issues.append(dict_issue)
```

### 把Json存放入文件

```python
def jsonlist2file(jsonlist, output_file):
    out_file = open(output_file, 'w')
    out_file.write(json.dumps(jsonlist, sort_keys=True, indent=4))
    out_file.close()
```

### 从文件读取Json

```python
def file2jsonlist(input_file):
    in_file = open(input_file, 'r')
    jsonlist = json.loads(in_file.read())
    in_file.close()
    return(jsonlist)
```



## refer

> 过程中参考过的重要文章/图书/模块/代码/...

[Online JSON Viewer](http://jsonviewer.stack.hu/) 

[JSON 数据格式 - SkySoot - 博客园](https://www.cnblogs.com/skysoot/archive/2012/04/17/2453010.html)  

[Python列表(list)、元祖(tuple)、集合(set)，和字典(dictionary)区别和联系 - 简书](https://www.jianshu.com/p/5ede7fa96d83) 



永远的: [提问的智慧](https://github.com/DebugUself/How-To-Ask-Questions-The-Smart-Way/blob/master/README-zh_CN.md)


## logging:

> 用倒序日期排列来从旧到新记要关键变化


- 200510 SS init.