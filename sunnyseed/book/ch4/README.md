# Ch4. 团队，协作

> 周任务汇报

**提醒:** 标题一般格式为:

    [ch4] <乐豆派@sunnyseed> (周末过去了🤮)周会说，分工协作才像团队

## 综述

> 当周所有任务完成整体情况小结





## 分析

> 如何理解任务的? 应该分几个步骤完成?

1. 清洗以往挖掘到的数据
2. 做top5（开课以来/近7天）
3. *发布网站
4. *如何通过 CI/CD (持续集成/部署) 功能令 gitlab 可以感知到小队分支变化


## 难点

> 预想/真实撞到的难点, 逐一先列出来

1. 用什么方式展现需要考虑

2. 附加题：如何通过 CI/CD (持续集成/部署) 功能，令gitlab 可以感知到小队分支变化，并自动化完成

   其中的第一步ci/cd是难点，可能还会遇到权限问题


## 追踪

> 用 IDD 形式来分层次动态规划, 追踪每个分解子任务的完成进展和 commit 证据

- [x] d0 接收任务 ~ 8cbc09a
  + [x] 破题
  + [x] 明确难点
- [x] d1 分解任务
  + [x] 子任务0 top5 ci -> @499b5aea
    + [x] 子任务1 top5 cc -> @499b5aea
  + [x] 子任务2 top5 is -> @362dc15e39e2d0ced968ba20a37b6262db6ea4c0
  + [x] 子任务3 top5 ic -> @362dc15e39e2d0ced968ba20a37b6262db6ea4c0
  + [x] 子任务4 CI/CD自动化 -> @966f504eb8b634f797b8ad7af53fd9a4adc3d901
- [x] d4 代码开发
  + [x] 框架
  + [x] 交互
  + [x] 数据输入
  + [x] 处理
  + [x] 友好输出
  + [x] 检验
- [x] d5 文档组织
  + [x] 使用手册
  + [x] 开发说明
  + [x] TODO
- [x] d6 演示准备
  + [x] 幻灯
  + [x] 截屏



## refer

> 过程中参考过的重要文章/图书/模块/代码/...

1.  [GitLab CI: Deployment & Environments | GitLab](https://about.gitlab.com/blog/2016/08/26/ci-deployment-and-environments/) 
2.  [什么是持续集成（CI）/持续部署（CD）？ - 知乎](https://zhuanlan.zhihu.com/p/42286143) 
3.  [Gitlab-CI使用教程 - 掘金](https://juejin.im/post/5e1965375188252695366f8b) 
4.  [微信开放文档](https://developers.weixin.qq.com/doc/offiaccount/Custom_Menus/Querying_Custom_Menus.html) 
5.  [开发者模式 | TNWX](https://javen205.gitee.io/tnwx/guide/wxcp/callback.html#%E9%85%8D%E7%BD%AE%E5%88%9D%E5%A7%8B%E5%8C%96) 
6.  [从零开始 Python 微信公众号开发 - 知乎](https://zhuanlan.zhihu.com/p/21354943) sae
7.  [Python微信公众号开发—小白篇（一） - Python提高班 - SegmentFault 思否](https://segmentfault.com/a/1190000015271082) 
8.  [Webhooks | GitLab](https://docs.gitlab.com/ee/user/project/integrations/webhooks.html) 
9.  [Python3.6创建虚拟环境并安装Flask框架 - 简书](https://www.jianshu.com/p/0b909759ccb3)
10.  [环境配置：anaconda使用配置说明 - 知乎](https://zhuanlan.zhihu.com/p/58541621) 
11.  [利用 GitHook 构建持续交付和部署 - CODING 博客](https://blog.coding.net/blog/GitHook-for-delivery-and-deployment) 
12.  [Python Pandas Cannot import QUOTE_MINIMAL - Stack Overflow](https://stackoverflow.com/questions/60912657/python-pandas-cannot-import-quote-minimal) 







永远的: [提问的智慧](https://github.com/DebugUself/How-To-Ask-Questions-The-Smart-Way/blob/master/README-zh_CN.md)


## logging:

> 用倒序日期排列来从旧到新记要关键变化


- 200517 SS init.