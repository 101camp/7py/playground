# ch2. 组队，欢乐的抄作业

> [ch2]d0.sunnyseed(分享🧐) 区分orphan分支，演示“如何正确抄作业”
>
> [ch2]d2.sunnyseed(修整😴) 写了info issue，早睡早起
>
> [ch2]d3.sunnyseed(同伴👻) 成功组队，完成issues=>notes俄罗斯套娃
>
> [ch2]d4.sunnyseed(hohoho🤪) 把playground主页，换成了我们组的介绍
>
> [ch2]d5.sunnyseed(累了😣) 花了不少功夫尝试自己做json，指定issue能半自动获取
>
> [ch2]d6.sunnyseed(逢山开路🥺)用plotly对小组issue数量作折线图

## 综述

#### [ch2]d0.sunnyseed(收获🤗) 找到orphan分支，演示“如何正确抄作业”

- ~ [#52 (closed)]()

#### [ch2]d2.sunnyseed(俄罗斯套娃🤡) projects=>issues=>notes

- 如何方便复用？把json主要功能分开，写了mydef.py，好import用

```
#备注可以用help看到
"""
jprint(jsonlist):
    打印 jsonlist
class2json(obj) => jsonlist: 
    把 class 脱壳为 jsonlist
jdetail(jsonlist, attr_name) => attr_list:
    通过 参数名字 获得 JSON 对象内，该参数的列表
glp(project_name)=>project:
    通过 project名称 获取 project 对象
json2xlsx(jsonlist, output_file):
    使用pandas存为xlsx
"""
...

# 仅测试时执行的写法
if __name__ == "__main__":
```

- 各 Issue 有几个回复？见ch1笔记
- 每个 Issue/回复, 分别是谁创建/回复的?这个就是一环套一环，一层层剥开就好：

```
for i in issues:
    # 把数据放入一个list 套 dict 的结构
    dict_issue = {}
    dict_issue['id'] = i.id
    ...
    list_issues.append(dict_issue)

    i_notes = i.notes.list(page= 0 ,per_page = max_issues)
    # 每个 Issue/回复, 分别是谁创建/回复的?
    for j in i_notes:
        # 人类发言
        if j.system == False:
            #d.jprint(j)
            dict_note = {}
            dict_note['id'] = j.id
            ...
            list_notes.append(dict_note)
```

- 直接把json存放入本地文件？必须再次夸奖pandas：

```
def jsonlist2file(jsonlist, output_file):
    out_file = open(output_file, 'w')
    out_file.write(json.dumps(jsonlist, sort_keys=True, indent=4))
    out_file.close()
```

- 取出json放入dataframe也容易。

```
    issues_frame = pd.read_json(from_file)
```

### [ch2]d5.sunnyseed(累了😣) 花了不少功夫尝试自己做json，指定issue能半自动获取

1. 存放入数据库

用pandas直接存放入sqlite数据库，有时会报错:

```
sqlite3.InterfaceError: Error binding parameter 1 - probably unsupported type.
```

- 注意 parameter 1 去看这个有没有问题

```
i_frame.to_sql("notes",engine)
```

1. 手工制作json = 把数据放入一个list 套 dict 的结构

```
for i in list_issues:
    # list_issues是 python-gitlab 的projectissue类
    # 把数据放入一个list 套 dict 的结构
    dict_issue = {
        "id" : i["id"],
        "iid" : i["iid"],
        "author_id" : i["author"]["id"],
        ...
    }
    new_issues.append(dict_issue)
```

1. 自动获取每个人的全部issue

```
import pandas as pd
import sqlite3

in_name = input("输入名字>")
after_date = input("最早创建日期 2020-05-05>")

conn = sqlite3.connect("7py.db")
df = pd.read_sql_query(f"""
                       select iid, title, links from issues
                       where author_name = '{in_name}'
                       and created_at >= '{after_date}';
                       """,
                       conn)

for index, row in df[:50].iterrows():
    print(f"{in_name}: {row['title']} ~ #{row['iid']}")
```

输出：

```
(base) ➜  SS_homework git:(sunnyseed) ✗ py ch2-autoget.py 
输入名字>xiongyin101
最早创建日期 2020-05-05>2020-04-04
xiongyin101: [ch2] xiongyin101 (进展比预想的快一丢丢） ~ #68
xiongyin101: [info]xiongyin101 能翻墙|英文OK|好合作|数据抓取 ~ #61
xiongyin101: [FAQ]python多版本操作的提醒事项 ~ #60
...
```

### [ch2]d6.sunnyseed(逢山开路🥺)用plotly对小组issue数量作折线图

- 先睡了，直接上结果图，明天再整理 [![plotly](README.assets/68747470733a2f2f747661312e73696e61696d672e636e2f6c617267652f30303753385a496c67793167656c686e377165726c6a33306a673063697134652e6a7067-20200516182026130.jpeg)](https://user-content.gitlab-static.net/253c9392025de54e372def5b966b53bf875366fc/68747470733a2f2f747661312e73696e61696d672e636e2f6c617267652f30303753385a496c67793167656c686e377165726c6a33306a673063697134652e6a7067)

## 分析

> 工程名: 学习通证
>
> 英文名: Proof of Learning
>
> 缩写名: PoL
>
> 整体目标:
>
> - 对课程涉及的各种资源进行一系列自动化监察/统计
> - 最终形成关键活跃度统计图表/排名/...
>
> 当周要求:
>
> - 获得所有课程 Issue 数据, 包含:
>   - 有多少 Issue
>   - 各 Issue 有几个回复
>   - 每个 Issue / 回复, 分别是谁创建/回复的?
> - 这种数据比较重要, 需要保存到本地,进一步分析:
>   - 所以, 请同时合理保存到本地
>   - 以合适的格式, 以便能随时从本地加载展开统计/分析
>
> > 额外任务, 若有余力, 也请完成
>
> - 当前笔记网站,使用 原始 html 来组织发布的
> - 尝试美化:
>   - 使用好看点儿的 CSS 模板?
>   - 换用其它简洁的 md -> html 工具?

- 如何理解任务的?

  **获得当前 task 仓库issue的细节，保存到本地**

- 应该分几个步骤完成?

1. 获得issue列表，进一步获得issue细节
2. 在本地存放/读取
3. 数据展示

## 难点

1. 怎么获得issue细节？怎么捣鼓Json？
2. 有哪些本地存放/读取方式，有什么差异？json/xlsx/sqlite

## 追踪

-  d0 接收任务
  -  破题
  -  明确难点
-  d1 分解任务
  -  获得issue列表
  -  获得对issue回复的note列表
  -  在本地存放/读取
  -  展现结果
-  d2 实验要点
  -  1 获得issue列表 ～  [Issues API | GitLab](https://docs.gitlab.com/ce/api/issues.html)
  -  2 获得对issue回复的note列表 ～ [Notes API | GitLab](https://docs.gitlab.com/ce/api/notes.html)
  -  3.1 json 存放/读取 ～ [Pandas 读写json|极客教程](https://geek-docs.com/pandas/pandas-read-write/pandas-reading-and-writing-json.html)
  -  3.2 sqlite 存放/读取
  -  3.3 xlsx 存放/读取
-  d3 决定方案
  -  1和2 试作 ~ [playground@95ecb085]()
  -  3.1和3.2 试做 ~ [playground@e6b78136]()
  -  3.3 试做 ~
-  d4 代码开发
  -  框架
  -  交互
  -  数据输入
  -  处理
  -  友好输出
  -  检验
-  d5 文档组织
  -  开发说明
-  d6 演示准备
  -  截屏
  -  幻灯

## refer

1. [Python调用Shell命令 - 简书](https://www.jianshu.com/p/5d999a668e79)
2. [Python split() 函数](https://blog.csdn.net/doiido/article/details/43204675)
3. [python-gitlab](https://python-gitlab.readthedocs.io/en/stable/install.html)
4. [Online JSON Viewer](http://jsonviewer.stack.hu/)     [在线JSON校验格式化工具（Be JSON）](https://www.bejson.com/)
5. [json-xls.com](http://www.json-xls.com/json2xls)
6. [使用SQLite - 廖雪峰的官方网站](https://www.liaoxuefeng.com/wiki/1016959663602400/1017801751919456)
7. [Pandas 读写json|极客教程](https://geek-docs.com/pandas/pandas-read-write/pandas-reading-and-writing-json.html)
8. [Python 和 Pandas 在 SQLite 数据库中的运用-云栖社区-阿里云](https://yq.aliyun.com/articles/226740)
9. [Issues API | GitLab](https://docs.gitlab.com/ce/api/issues.html)
10. [Notes API | GitLab](https://docs.gitlab.com/ce/api/notes.html)
11. [CSS](https://www.w3schools.com/css/css_align.asp)
12. [HTML](https://www.w3school.com.cn/tags/tag_img.asp)
13. [DM5: 咩是团队? — 蟒营™ 怂怼录](https://blog.101.camp/dm/190822-what-is-team/)
14. [GitLab Pages examples / plain-html · GitLab](https://gitlab.com/pages/plain-html)
15. [DB Browser for SQLite](https://sqlitebrowser.org/dl/)
16. [JSON 数据格式 - SkySoot - 博客园](https://www.cnblogs.com/skysoot/archive/2012/04/17/2453010.html)
17. [Python列表(list)、元祖(tuple)、集合(set)，和字典(dictionary)区别和联系 - 简书](https://www.jianshu.com/p/5ede7fa96d83)
18. [SQLite 日期 & 时间 | 菜鸟教程](https://www.runoob.com/sqlite/sqlite-date-time.html)
19. 

其他

[Kaggle入门](https://zhuanlan.zhihu.com/p/25686876)

[利用plotly for Python进行数据可视化 - 知乎](https://zhuanlan.zhihu.com/p/26461072)

[COVID-19 Open Research Dataset Challenge (CORD-19) | Kaggle](https://www.kaggle.com/allen-institute-for-ai/CORD-19-research-challenge/kernels?sortBy=hotness&group=everyone&pageSize=20&datasetId=551982&outputType=Visualization)

永远的: [提问的智慧](https://github.com/DebugUself/How-To-Ask-Questions-The-Smart-Way/blob/master/README-zh_CN.md)

## logging:

> 用倒序日期排列来从旧到新记要关键变化

- 200504 SS init.