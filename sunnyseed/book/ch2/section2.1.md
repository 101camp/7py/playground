### ch2. hohoho🤪 我们是有主页的小组

### [点击打开乐豆派主页](https://101camp.gitlab.io/7py/playground/)

8点听了答疑会，有两个有意思的问题：

```
问题2，Erica: 抄作业却看不懂，不要紧吧？
Dama:
看疗效即可。
最基本只要知道，怎么运行，怎么调试就行了。(Erica:说到了w3cschool，缓存问题。。。后面马上都遇到了）

问题3，sunnyseed：网页展示要准备些什么？
要看你想要什么，静态？动态？telnet？第三周有静态网页的内容。
首先把课程学好。
```

课后看附加题：“当前笔记网站，使用原始 html 来组织发布的，尝试美化”，想着哪里有什么笔记网站，搜“gitlab 显示html”，结果找到了gitlab的一个例子：[GitLab Pages examples / plain-html · GitLab](https://gitlab.com/pages/plain-html) 😍如获至宝

1. 一个目录三个文件
2. 修改index.html:`html 使用成对的标签标记内容`

```
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="generator" content="GitLab Pages">
    <title>Plain HTML site using GitLab Pages</title>
    <link rel="stylesheet" href="style.css">
  </head>
  <body>
    <div class="navbar">
      <a href='https://gitlab.com/sunnyseed'>sunnyseed</a>
      <a href='https://gitlab.com/xiongyin101'>xiongyin</a>
      <a href='https://gitlab.com/jiaqili'>jiaqili</a>
    </div>

    <h1>Hello World!</h1>
    <h2>乐豆派</h2>
    
    <p>想写什么写什么</p>

  </body>
</html>
```

1. 修改style.css:`对html 的标签，进行格式化`

```
body {
  font-family: sans-serif;
  margin: auto;
  max-width: 800px;
}

h1 {
  background-color:DodgerBlue;
  color: white;
}

.navbar {
  background-color: #313236;
  border-radius: 2px;
  max-width: 800px;
}
```

### 