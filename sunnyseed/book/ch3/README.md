# ch3. 一半是火焰，一半泼了水

> 周任务汇报

**提醒:** 标题一般格式为:

    [ch3]乐豆派@sunnyseed: 冷汗的平方😓，感觉这难度，是蹭蹭上涨
    [ch3]乐豆派@sunnyseed: 聚焦🤕，终于完成了分页和获取commit增删改，得保持节奏,问题是为什么只有master?
    [ch3]乐豆派@sunnyseed: 每天进步一点儿🤏，完成task1，问题4发现一个没有diff的commit
    [ch3]乐豆派@sunnyseed: 坚持😰，熊小姐对mac泼了水=团队战力对折，课内作业完成，学习咖啡组代码

## 综述

#### 作业链接

1. [jupyter notebook 版本](https://gitlab.com/101camp/7py/tasks/-/blob/sunnyseed/logging/ch3-tasks.ipynb)

2. [Py 版本](https://gitlab.com/101camp/7py/tasks/-/blob/sunnyseed/logging/ch3-tasks.py)

   

#### 问题1. requests超过100，如何分页？

```
关键就是要获得head，有两种方式：
1. requests.head 注意这时的 token 要有write权限。
curl --head --header "PRIVATE-TOKEN:<???>" https://gitlab.com/api/v4/projects/17730311/repository/commits?per_page=10&page=1
2. requests.get 其实一样可以取得 headers
```

```python
def get_url_pages(session, url, headers, params):
    #获得所有页，返回json列表
    params['page'] = '1'
    r = session.get(url, headers = headers, params=params)
    r_list = r.json()
  
    #获取几次？
    get_times = int(r.headers['X-Total']) // int(params['per_page']) 
    
    for i in range(get_times):
        params['page'] = str(i + 2)
        r = (session.get(url, headers = headers, params=params))
        r_list.extend(r.json()) 
        
    return(r_list)
  
#不用session也可以，没看出明显区别
session = requests.session()
headers['PRIVATE-TOKEN'] = 'xxx'
params = {
    'per_page' : '10',
    'all' : 'True'
}
url = 'https://gitlab.com/api/v4/projects/17730311/repository/commits'
commits_list = get_url_pages(session, url, headers, params)
```

#### 问题2. 如何获取commit，删除/变更了几行？修改了几个文件？

```
### 获取 single commits : 删除了几行，变更了几行
curl --header "PRIVATE-TOKEN:-43KkjBS1U95zin8pfW7" "https://gitlab.com/api/v4/projects/17730311/repository/commits/db5e0318606f3be0c96f52da3371885ebd382676"
```

```
### 获取 commits diff : 修改了几个文件
curl --header "PRIVATE-TOKEN:-43KkjBS1U95zin8pfW7" "https://gitlab.com/api/v4/projects/17730311/repository/commits/db5e0318606f3be0c96f52da3371885ebd382676/diff"
```

这就是怎么分析api内容了，仔细些，多试几次。

#### 问题3. 是为什么只拿到了master? 

| Attribute      | Type           | Required | Description                                                  |
| :------------- | :------------- | :------- | :----------------------------------------------------------- |
| `id`           | integer/string | yes      | The ID or [URL-encoded path of the project](https://docs.gitlab.com/ee/api/README.html#namespaced-path-encoding) owned by the authenticated user |
| `ref_name`     | string         | no       | The name of a repository branch, tag or revision range, or if not given the default branch |
| `since`        | string         | no       | Only commits after or on this date will be returned in ISO 8601 format YYYY-MM-DDTHH:MM:SSZ |
| `until`        | string         | no       | Only commits before or on this date will be returned in ISO 8601 format YYYY-MM-DDTHH:MM:SSZ |
| `path`         | string         | no       | The file path                                                |
| `all`          | boolean        | no       | Retrieve every commit from the repository                    |
| `with_stats`   | boolean        | no       | Stats about each commit will be added to the response        |
| `first_parent` | boolean        | no       | Follow only the first parent commit upon seeing a merge commit |
| `order`        | string         | no       | List commits in order. Possible values: `default`, [`topo`](https://git-scm.com/docs/git-log#Documentation/git-log.txt---topo-order). Defaults to `default`, the commits are shown in reverse chronological order. |

两个方案1. ref_name='sunnyseed' 2. all=True

在这里，params有多种写法，都可以：

```python
params = (
      ('per_page', '10'),
  		('all', 'True'),
)
```

建议写成字典，后续容易操作

```python
params = {
    'per_page' : '10',
    'all' : 'True'
}
```

#### 问题4. 为什么个别commit没有diff？

目前有一个commit没有diff，是我写的，但没看出它和别人有啥不同:

3eb1cb26117514559caed77a38c6b63fca27ddd6

结果发现还真的是，没有diff 

[[提问] 为什么个别 commit，用diff 查不到 修改的内容？ (#86) ](https://gitlab.com/101camp/7py/tasks/-/issues/86) 

#### 问题5. 相互讨论的 Commit-Comments在哪儿？

组内的熊小姐独辟蹊径，发现了events
`curl --header "PRIVATE-TOKEN:-43KkjBS1U95zin8pfW7" https://gitlab.com/api/v4/projects/17730311/events?action=commented&scope=all`
其中:"noteable_type": "Commit"
可惜遍寻不着commit id

所以还是笨办法，遍历吧，在discussions里面
https://docs.gitlab.com/ee/api/discussions.html#list-project-commit-discussion-items

GET /projects/:id/commits/:commit_id/discussions
b5133011c59818a4ed785cc87891e7528302e257

curl --header "PRIVATE-TOKEN:-43KkjBS1U95zin8pfW7" "https://gitlab.com/api/v4/projects/17730311/repository/commits/b5133011c59818a4ed785cc87891e7528302e257/discussions"

#### 问题6. 如何使用 ini 文件隐藏private_token?

这个看咖啡组说明即可  [[Team] 冷萃咖啡(Cold Brew) config.ini使用指南 (#87) ](https://gitlab.com/101camp/7py/tasks/-/issues/87) 

就此基本都完成了。



## 分析

> 1. 本周进一步:
>
> - 探索获得课程以来所有分支学员的所有 commit 数据:
>   - 何时
>   - 由谁
>   - 可选:
>     - 删除了几行
>     - 变更了几行
>     - 修改了几个文件
> - 以及相互讨论的 Commit-Comments 数据:
>   - 何时
>   - 由谁
>   - 可选:
>     - 针对哪行代码/文字
>     - 写了多少字 Comments?
>     - 变更了几行
>
> 2. 团队任务
>
> - 团队笔记应该开始周报的发布（在队长笔记网站中）
>
> - 并在 tasks 维基仓库中
>
>   - `logging/` 目录中
>
>   - 创建团队手册
>
>   - ```
>     Team[团队缩写].md
>     ```

如何理解任务的? 应该分几个步骤完成?

1. 获取commit ~ https://docs.gitlab.com/ee/api/commits.html#get-a-single-commit
2. 获取Commit-Comments ~ https://docs.gitlab.com/ee/api/discussions.html#commits
3. [团队] 队长笔记网站中发布周报
4. [团队] tasks 维基仓库中创建团队手册


## 难点

1. 获取commit不难，但怎么知道删除、修改的行数，和文件数？
2. Commit-Comments 的 api 是什么，怎么分析？
3. 怎么分页？
4. 怎么简单的获得所有有discussion的commit id？

团队任务的难点：

1. gitbook.io
2. Wiki 下的 logging 目录？


## 追踪

> 用 IDD 形式来分层次动态规划, 追踪每个分解子任务的完成进展和 commit 证据

- [x] d0 接收任务
  + [x] 破题
  + [x] 明确难点
- [x] d1 分解任务
  + [x] 1. 获取commit
  + [x] 2. 获取Commit-Comments
  + [ ] 3. [团队] 队长笔记网站中发布周报
  + [x] 4. [团队] tasks 维基仓库中创建团队手册
- [x] d2 实验要点
  + [x] 1. 获取commit
  + [x] 2. 获取commit 细节
  + [ ] 3. [团队] 队长笔记网站中发布周报
  + [x] 4. [团队] tasks 维基仓库中创建团队手册
- [x] d3 决定方案
  + [x] 功能1试作 ~ 57cde4b871123b062b5b32ee7875b0e7b388a183
  + [x] 功能2试作 ~ ba4035596c9d9009eb55643cc40456103d8a89ce
  + [ ] 功能3试做
  + [x] 功能4试做 ~ 04930480579fc904a27321189a9446cd8ffaab33
- [x] d4 代码开发
  + [x] 框架
  + [x] 交互
  + [x] 数据输入
  + [x] 处理
  + [x] 友好输出
  + [x] 检验
- [x] d5 文档组织
  + [x] 开发说明
- [ ] d6 演示准备
  + [ ] 幻灯
  + [ ] 截屏



## refer

> 过程中参考过的重要文章/图书/模块/代码/...

1. [Convert cURL command syntax to Python requests](https://curl.trillworks.com/#python) 
2. [Commits API | GitLab](https://docs.gitlab.com/ee/api/commits.html) 
3. Google : requests.head python 3 ~  [Python请求头方法](https://www.w3schools.com/python/ref_requests_head.asp)  `这个绕远路了，其实不需要用requests.head`
4. [Requests: 让 HTTP 服务人类 — Requests 2.18.1 文档](https://requests.readthedocs.io/zh_CN/latest/) 
5. https://docs.gitlab.com/ee/api/discussions.html#commits
6. [python json dump 输出中文 - 李涛的博客 | Tao's Blog](http://litaotju.github.io/python/2016/06/28/python-json-dump-utf/) `让dump输出真正的中文`
7.  [Image uploading does not work in comments (gitlab.com) (#38721) ](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/38721)`gitlab上传图片,简单到意想不到`

8. [Requests ConnectionError: ('Connection aborted.', OSError("(104, 'ECONNRESET')"](https://stackoverflow.com/questions/43165341/python3-requests-connectionerror-connection-aborted-oserror104-econnr) `不明觉厉反正解决了`

9.  [Python中的空值判断 - 简书](https://www.jianshu.com/p/a0d273550f70) `None`

10.  [session用法 — Requests 2.18.1 文档](https://requests.readthedocs.io/zh_CN/latest/user/advanced.html) `这个高档了，有用处吗？`

11.  [Python List extend()方法 | 菜鸟教程](https://www.runoob.com/python/att-list-extend.html) `遇到两个json合并，不应该用append`

12.  [Python截取字符串（字符串切片）方法详解](http://c.biancheng.net/view/2178.html) 

13.  [Git忽略规则：.gitignore配置 | 木凡博客](https://mounui.com/193.html) `效果待验证`

14.  [[Python] 获取当前路径 - 简书(不起作用)](https://www.jianshu.com/p/d168e8ccdd6d) `暂时无效`

15.  [file does not exist in Jupyter Notebook（无效）](https://stackoverflow.com/questions/39125532/file-does-not-exist-in-jupyter-notebook) 

16.  [Python Requests - No connection adapters - Stack Overflow](https://stackoverflow.com/questions/15115328/python-requests-no-connection-adapters) `把https，写成htts的后果是浪费15分钟`

    



其他

1. [Welcome to Invoke! — Invoke documentation](http://www.pyinvoke.org/) 
2.  [Python 输出命令行进度条 - 知乎](https://zhuanlan.zhihu.com/p/53611310) 

永远的: [提问的智慧](https://github.com/DebugUself/How-To-Ask-Questions-The-Smart-Way/blob/master/README-zh_CN.md)


## logging:

> 用倒序日期排列来从旧到新记要关键变化


- 200511 SS init.