> 蟒营™学习笔记

    [note] 乐豆派@sunnyseed: CI续集，怎么把大象🐘放进冰箱？

## 背景
> 上集讲到，CI太高冷，只对commit事件才有反应，不如Webhook平易近人，于是乐豆派@葵花籽先生追求Webhook去了。回头一看，发现团队笔记网站的自动发布，还得靠CI。

- Webhook触发后，想用脚本自动git push到gitlab，失败概率好高，10个try也架不住，遂放弃
- 研究CI : 代码变更后 -> 触发 -> 自动启动一个管道，去完成某些任务
  - 在这里 = 手工push上传md文件 ->触发-> 
    1. 把md  Build成 gitbook
    2. 再发布到public目录

> 看着就是真命天子😍


## 进展
#### 1. CI 的 MVP

> 过程看[[Ask\] CI 的第一次尝试：成功，但public里没有文件<解决了，网站缓存问题😭>](https://gitlab.com/101camp/7py/tasks/-/issues/94) ～ #94 ，简单说就是在master根目录下加入一个[**.gitlab-ci.yml**](https://gitlab.com/101camp/7py/playground/-/blob/master/.gitlab-ci.yml)

```yaml
#使用的docker镜像
image: alpine:latest

pages:
  stage: deploy
  script:
  # 命令
  - cp ./sunnyseed/web/*.html ./public/
  artifacts:
    paths:
    - public
  only:
  - master

```

关键有两句

1. `cp ./sunnyseed/web/*.html ./public/`，这句就是命令行，把html文件拷贝进发布目录

2. `image: alpine:latest`，用这个docker镜像，来运行上面这句语句。

   > 什么？可以直接用docker运行cli，甚至能运行python？这跨度太大，我跟不上

#### 2. Gitbook 的 MVP

> 这个更简单粗暴，但真好用
>
> 2个文件 + 2句命令搞定，具体看网上的介绍吧

```
gitbook init
gitbook build
```

#### 3. 拼装

> 这个逻辑看着挺简单的，就是👇
>
> 第一张没有用CI前

![](https://tva1.sinaimg.cn/large/007S8ZIlgy1gf3ay7mzrdj316y0u00yo.jpg)

> 第二张使用CI后

![](https://tva1.sinaimg.cn/large/007S8ZIlgy1gf3ay8f7gvj316y0u0jxz.jpg)

> 可以看出就是用CI，做了手工做的事情
>
> 1. build
>
> 2. copy
>
> 但要把写完的脚本用上却不容易，总是遇到cancel或者pending，不起作业，也没有报错信息

### 报错信息在哪里？

> 遍寻网络，尝试修改了五六种不同脚本，包括官网的例子之后。终于遇到一个正确能用的：

```yaml
# requiring the environment of NodeJS 10
image: node:10

# add 'node_modules' to cache for speeding up builds
cache:
  paths:
    - node_modules/ # Node modules and dependencies

before_script:
  - npm install gitbook-cli -g # install gitbook
  - gitbook fetch 3.2.3 # fetch final stable version
  - gitbook install # add any requested plugins in book.json


# the 'pages' job will deploy and build your site to the 'public' path
pages:
  stage: deploy
  script:
    - gitbook build ./sunnyseed/book public # build to public path
  artifacts:
    paths:
      - public
    expire_in: 1 week
  only:
    - master # this job will affect only the 'master' branch

```

> 终于看到passed，激动。

![image-20200524100135421](https://tva1.sinaimg.cn/large/007S8ZIlgy1gf3b70knlaj31070u0djy.jpg)

> 怎么把大象放进冰箱？
>
> 1. 知道大象能放进冰箱
> 2. 找到冰箱
> 3. 学会开门的方法
> 4. 实践它

#### 使用手册

- Playground, master分支，sunnyseed/book目录，按照gitbook格式放入md文件，修改目录
- git push
- 就可以看到结果 ～ [乐豆派主页](https://101camp.gitlab.io/7py/playground/)

  


## 问题
> 详细描述问题的范畴/条件/上下文/...

- Ci怎么调试
- docker怎么使用


## refer
> 过程中参考过的重要文章/图书/模块/代码/...

永远的: [提问的智慧](https://github.com/DebugUself/How-To-Ask-Questions-The-Smart-Way/blob/master/README-zh_CN.md)

 [.gitlab-ci.yml · master · GitLab Pages examples / gitbook · GitLab](https://gitlab.com/pages/gitbook/-/blob/master/.gitlab-ci.yml) 




## logging:
> 用倒序日期排列来从旧到新记要关键变化


- 200524 SS init.