# 乐豆派

乐豆派是一个集中展示乐高视频的网站。

它既是[蟒营](https://py.101.camp) 第七期的小组项目作业， 也是对编程信心和自学能力的一次验证和展现。

Learn Python, Learn how to learn, Have fun.

## 内容列表

- [背景](#背景)
- [网址](#网址)
- [原型交互](#原型交互)
- [核心功能](#核心功能)
- [代码](#代码)
- [开发小组](#开发小组)
- [日志](#日志)

## 背景

乐豆派是[蟒营](https://py.101.camp) 第七期的项目作业。

学习是件有趣的事情。

玩也是件有趣的事情。

把我们爱玩的乐高通过数据抓取、清洗、分类展现在网络环境中， 其中涉及到的工作既可以加强前几周已经学到的东西， 还可以生发出一些新的探索内容，例如网络端视频的呈现、网站原型及业务流程的梳理。 更重要的是，乐豆派将会把以上的一切串联起来，成为一个使用python编程语言实现的可以真实使用的的作品。

## 网址

## 原型交互

### 登录页

[![登陆页](https://gitlab.com/101camp/7py/tasks/uploads/246e358cef44aab7249f944382267a3e/%E9%A2%91%E9%81%93%E9%A1%B5.jpeg)](https://gitlab.com/101camp/7py/tasks/uploads/246e358cef44aab7249f944382267a3e/频道页.jpeg)

### 频道页

[![频道页](https://gitlab.com/101camp/7py/tasks/uploads/246e358cef44aab7249f944382267a3e/%E9%A2%91%E9%81%93%E9%A1%B5.jpeg)](https://gitlab.com/101camp/7py/tasks/uploads/246e358cef44aab7249f944382267a3e/频道页.jpeg)

（今天做文档的时候感觉频道页多余了，应该直接从登录页进入视频单页的，具体可以小组会再讨论）

### 视频页

[![视频页](https://gitlab.com/101camp/7py/tasks/uploads/0b19fc285e96df4f8d1c1acc131d13b9/%E8%A7%86%E9%A2%91%E9%A1%B5.jpeg)](https://gitlab.com/101camp/7py/tasks/uploads/0b19fc285e96df4f8d1c1acc131d13b9/视频页.jpeg)

## 核心功能

### 前台

1. 视频分类：在乐豆派，来自bilibili和youtube的乐高主题视频将分为五个类别：测评、教程、自创、影视、故事，并分别在主页面、频道页和单独视频页面呈现。
2. 视频点评：浏览者无须注册，即可对任一视频留言点评并评分；(ss:但留言需经审核后才能展示)
3. 视频排序：视频评分将决定该视频在首页和分类页面的展示顺序。

### 后台

1. 自动获取：自动跟踪bilibili部分up主，和最热lego视频，自动发布
2. 自动评分：根据视频标题等信息，自动初始分类
3. 维护页面：有一个简单的管理员页面

### 数据结构

1. Video.table

['视频序号', '作者', '标题', '视频时长', '投稿日期', '播放量', '试看链接', '视频出处', '评分','分类']

['vid'(int),'author','title','length'(int:秒数),'created_at'(unix timestamp:秒数),'view_count'(int),'link','refer','rank'(int),'type'(int)]

1. Comment.table

['评论序号','视频序号','作者','标题','评论内容','审核标记']

['iid'(int),'vid'(int),'作者','标题','评论内容','审核标记'(int:默认=0，审核通过=1)]

### 代码

1. bilibili视频数据抓取
   - search video list & download-> [68bbbbb7]()
   - up主 video list -> [68bbbbb7]()
   - video detail -> [4f53e1fb]()
2. 数据清洗 -> 079e6db8a53bee48cb96a6783f4ed135b253adab
3. 网页展现 -> https://github.com/sunnyseed/bluelog

### 安装

clone:

```
$ git clone https://github.com/sunnyseed/bluelog.git
$ cd bluelog
```

create & activate virtual env then install dependency:

with conda + pip: 

```
$ pip install -r requirements.txt
```

or with Pipenv:

```
$ pipenv install --dev
$ pipenv shell
```

generate fake data then run:

```
$ flask forge
$ flask run
* Running on http://127.0.0.1:5000/
```

Test account:

* username: `admin`
* password: `helloflask`

修改数据库结构，如果遇到 sqlalchemy migration error：target database is not up to date

```
flask db heads
flask db current
# 发现两者不同
flask db stamp heads
flask db migrate
# 检查迁移脚本
flask db upgrade
```

使用 httpie 测试 api

```
# get
http --json Get http://127.0.0.1:5000/api/v1/posts/
http --json Get http://127.0.0.1:5000/api/v1/posts/100

# post
http --json POST \
http://127.0.0.1:5000/api/v1/posts/ \
title=legomaster body=testapi category_id:=1 \
pic_url=BV1Gs411Q7mw.jpg bvid=BVtest stars:=0 username=test aid=test \
cid=test vid_url=test
```

使用 ngrok

```
flask run --host 0.0.0.0 --port 5000
cd ~
./ngrok http -subdomain=sunnyseed 5000
```



## 开发小组

- 小组主页 https://101camp.gitlab.io/7py/playground/
- 组长[@sunnyseed](https://gitlab.com/sunnyseed)
- 组员[@jiaqili](https://gitlab.com/jiaqili)
- 组员[@xiongyin101](https://gitlab.com/xiongyin101)

## refer

bilibili api

-  [bilibili-api 1.3.4](https://pypi.org/project/bilibili-api/)
-  [python多线程爬取b站视频](https://www.bilibili.com/video/BV1XE411f7Ai?from=search&seid=17244053401463109431)

Flask + bootstrap

- [Flask web 开发](https://github.com/fjavieralba/flasky)

- [Flask Web 开发实战](http://helloflask.com/book)

## 日志

- 2006
- 200526 xiongyin101 创立文档

