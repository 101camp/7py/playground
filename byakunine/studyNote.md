200405

Sijia Wu@BYAKUNINE MINGW64 ~/Documents/Academic/PYTHON/Gitlab/playground (master)
$ git checkout byakunine
fatal: 'byakunine' could be both a local file and a tracking branch.
Please use -- (and optionally --no-guess) to disambiguate

Sijia Wu@BYAKUNINE MINGW64 ~/Documents/Academic/PYTHON/Gitlab/playground (master)
$ git checkout --byakunine
error: unknown option `byakunine'
usage: git checkout [<options>] <branch>
   or: git checkout [<options>] [<branch>] -- <file>...

    -b <branch>           create and checkout a new branch
    -B <branch>           create/reset and checkout a branch
    -l                    create reflog for new branch
    --guess               second guess 'git checkout <no-such-branch>' (default)
    --overlay             use overlay mode (default)
    -q, --quiet           suppress progress reporting
    --recurse-submodules[=<checkout>]
                          control recursive updating of submodules
    --progress            force progress reporting
    -m, --merge           perform a 3-way merge with the new branch
    --conflict <style>    conflict style (merge or diff3)
    -d, --detach          detach HEAD at named commit
    -t, --track           set upstream info for new branch
    -f, --force           force checkout (throw away local modifications)
    --orphan <new-branch>
                          new unparented branch
    --overwrite-ignore    update ignored files (default)
    --ignore-other-worktrees
                          do not check if another worktree is holding the given ref
    -2, --ours            checkout our version for unmerged files
    -3, --theirs          checkout their version for unmerged files
    -p, --patch           select hunks interactively
    --ignore-skip-worktree-bits
                          do not limit pathspecs to sparse entries only
    --pathspec-from-file <file>
                          read pathspec from file
    --pathspec-file-nul   with --pathspec-from-file, pathspec elements are separated with NUL character
Sijia Wu@BYAKUNINE MINGW64 ~/Documents/Academic/PYTHON/Gitlab/playground (master)
$ git checkout -b byakunine
Switched to a new branch 'byakunine'



