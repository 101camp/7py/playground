# ch1. 长得像json，叫起来像json，跑起来也像json

> [ch1]d0.sunnyseed(oyo) 一滴冷汗
>
> [ch1]d1.sunnyseed(怀疑🤔) 已获取orphan
>
> [ch1]d2.sunnyseed(嗯哼！) 使用api获取orphan
>
> [ch1]d3.sunnyseed(补课😀) lpthw追e33到e39
>
> [ch1]d4.sunnyseed(绕了一大圈🙄)格式化api返回
>
> [ch1]d5.sunnyseed(早😜)长得像json，叫起来像json，跑起来也像json

## 综述

#### [ch1]d1.sunnyseed(oyo) 一滴冷汗

> 看到本周题目，心想wtf

> 读了几遍[ch1: CLI 是元袓 MVP](https://gitlab.com/101camp/7py/tasks/-/blob/master/ch1.md)，先去查查什么是cli，原来就是命令行的意思。叮咚！大妈你这暗示太明显了，立刻就想到：

1. 命令行 git branch -a 获得输出到文件： [Python调用Shell命令](https://www.jianshu.com/p/5d999a668e79)

   ```python
   >>> import os
   >>> os.system("git branch -a > userlist")
   0
   ```

2. 读取文件后格式化，很快完成MVP：[Python split() 函数](https://blog.csdn.net/doiido/article/details/43204675) 

   ```python
   # str.split(str="",num=string.count(str))[n]
   u = "www.doiido.com.cn"
   # 分割两次，并取序列为1的项
   print u.split('.',2)[1]
   
   ```


#### [ch1]d2.sunnyseed(嗯哼！) 使用api获取orphan

> 又读了一遍题目`对课程涉及的各种资源进行一系列自动化监察/统计`，于是去搜索`Python爬虫需要哪些知识？`:

1. Lpthw e1-e40
2. Html 和 [json](https://www.runoob.com/json/json-intro.html)，大致看了下json，是一种类似字典的数据格式

> 搜索`python gitlab 爬虫`，发现需要使用 `gitlab api`，有一个封装好的python库：`python-gitlab`，看了文档 [python-gitlab](https://python-gitlab.readthedocs.io/en/stable/install.html) ，看了栗子，不复杂，三步：

```python
import gitlab
# 1. private token or personal token authentication
gl = gitlab.Gitlab('https://gitlab.com/', private_token='****')
gl.auth()
# 2. 通过 "空间名/项目""名获取 project 对象
project = gl.projects.get('101camp/7py/playground')
# 3. Get the list of branches for a repository:
branches = project.branches.list()
print(branches)
```

执行结果：

```python
[<ProjectBranch name:byakunine>, <ProjectBranch name:hll101>, <ProjectBranch name:sunnyseed>, ...
```



#### [ch1]d3.sunnyseed(补课😀) lpthw追e33到e39

> 决定先去把lpthw关于字典的几课补上

> 从同学的issue，发现了[erica师姐的笔记](https://gitlab.com/101camp/7py/tasks/-/wikis/IMHO/200330-101camp6py-erica-ch1d1) ，这小抄太厉害了，好多关键点。虽然用的是invoke，但json部分应该一样，先记下来。



#### [ch1]d4.sunnyseed(绕了一大圈🙄)格式化api返回

> 取得的数据长得像json，叫起来像json，跑起来确不像json。
>
> 网上兜兜转转，踩了三小时坑，半夜果然不适合我，第二天一早解决： 

1. 研究print(branches[0])的返回，其中像json的部分是：

   ```python
   'name':'byakunine',
   'commit':{
   		'id':'c8d0b1a5e9b038ade1b846c3153fb3748ec79dfd',
   		'short_id':'c8d0b1a5',
   		'created_at':'2020-03-30T07:35:00.000+00:00',
   		'parent_ids':None,
   ...
   ```

   用erica笔记里的代码，把这部分测试了一下：

   ```python
   >>> import json
   >>> def jprint(obj):
   ...     # create a formatted string of the Python JSON object
   ...     text = json.dumps(obj, sort_keys=True, indent=4)
   ...     print(text)
   ... 
   >>> jsontxt = [{'name': 'LPTHW', 'commit': {'id':...
   >>> jprint(jsontxt)
   ```

   能过😀。猜测dumps的意思就是：格式化显示json

2. 开头多了`<class 'gitlab.v4.objects.ProjectBranch'> `显然是类，python-gitlab没找到说明，用dir()列出属性，找到'attributes'

   ```python
   >>> type(branches)
   <class 'list'>
   >>> type(branches[0])
   <class 'gitlab.v4.objects.ProjectBranch'>
   >>> dir(branches[0])
   ['__class__', ... , '_updated_attrs', 'attributes', 'delete', 'get_id', 'manager', 'protect', 'unprotect']
   >>> branches[0].attributes
   {'name': 'byakunine', 'commit': {'id': 'c8d0b1a5e9b038ade1b846c3153fb3748ec79dfd', 'short_id': 'c8d0b1a5', 'created_at': ... 
   ```

   后面就简单了

   ```python
   >>> for i in range[0, len(branches)]:
   ...     jsontxt.append(branches[i].attributes)
   >>> for j in jsontxt:
   ...     print(j['name'])
   ```

   > 这里可以看出一点，json格式，就是列表[]里面，嵌套字典{}

#### [ch1]d5.sunnyseed(早😜)长得像json，叫起来像json，跑起来也像json

> 哪些是orphan分支? 转excel看着方便些

 [现在手里有一堆JSON格式的数据，如何转化成excel？ - 知乎](https://www.zhihu.com/question/265720343) 

1. 在线转 [json-xls.com](http://www.json-xls.com/json2xls) 

2. python转，就2句，强大了我的熊猫🐼

```python
>>> import pandas
>>> # pandas.read_json("input.json").to_excel("output.xlsx") 
>>> pandas.read_json(json.dumps(jsontxt, sort_keys=True, indent=4)).to_excel("output.xlsx")
```

这里有一点，input.json，其实就是格式化/dumps的json

> 什么是orphan分支？orphan = 没有commit历史的“新”分支

1. 自己制造普通分支作实验材料🤪

```
(base) ➜  orphan git:(sunnyseed) git checkout -b sunnyseed-try 
(base) ➜  orphan git:(sunnyseed-try) git push --set-upstream origin sunnyseed-try

(base) ➜  playground git:(master) git checkout -b sunnyseed-try-cli
(base) ➜  playground git:(sunnyseed-try-cli) git push origin sunnyseed-try-cli:sunnyseed-try-cli
```

2. 怎么获得某一个分支的commit历史？第一个commit和master不同的是orphan。关键code：

> Cli 模式下：

```
(base) ➜  heads git:(sunnyseed-try) git rev-list --max-parents=0 HEAD
(base) ➜  heads git:(sunnyseed-try) git log —reverse
```

如果同名checkout：

```
(base) ➜  playground git:(master) git checkout sunnyseed -- 
```

> python code:

```python
commits = project.commits.list(ref_name='master',page= 0 ,per_page = 150)
master_init_commit = commits[-1].id
```

说明见refer。



## 分析

> 工程名: 学习通证
>
> 英文名: Proof of Learning
>
> 缩写名: PoL
>
> 整体目标:
>
> - 对课程涉及的各种资源进行一系列自动化监察/统计
> - 最终形成关键活跃度统计图表/排名/...
>
> 当周要求:
>
> - 可运行
> - 输入指令,可以获得当前 task 仓库有多少学员 orphan 分支
>   - 以便作为进一步行为监察的基础数据

- 如何理解任务的? 

  **获得当前 task 仓库有多少学员 orphan 分支**

- 应该分几个步骤完成?

1. 获得orphan分支
2. 整理数据
3. 展示数据


## 难点

1. 获得全部orphan分支，命令行？API？爬虫？
2. 怎么格式化数据？


## 追踪

- [x] d0 接收任务
  + [x] 破题
  + [x] 明确难点
- [x] d1 分解任务
  + [x] 获得orphan分支
  + [x] 整理数据
  + [x] 展示数据
  + [x] 确认数据
- [x] d1 实验要点
  + [x] 1.1 命令行获得orphan分支 ~ `git branch -a`
  + [x] 1.2 整理数据 ~ `list.split()` 
  + [x] 2.1 API 获得orphan分支 ~ `import gitlab`
  + [x] 2.2 整理数据 ~`dir(class)`
  + [x] 2.3 确认数据，哪些是orphan分支
- [x] d2-d4 决定方案
  + [x] 1.1 试作 ~ playground@ee2006a70bb48d5de40394deea09eb21b27fa3bf 里面的ch1.py
  + [x] 1.2 试作 ~ playground@20af61606f88eb1d05a598819438f39121e10cb3
  + [x] 2.1 试做 ~ playground@d4963b3578577fa431ffd3023c29232a09aa1328
  + [x] 2.2 试做 ~ playground@b7f94a38b2468189254ad7e612cc096cf8188e1f
  + [x] 2.3 试做
- [x] d2-d4 代码开发
  + [x] 框架
  + [x] 交互
  + [x] 数据输入
  + [x] 处理
  + [x] 友好输出
  + [x] 检验
- [x] d5 文档组织
  + [x] 开发说明
- [x] d6 演示准备
  + [x] 截屏
  + [x] 幻灯



## refer

1. [Python调用Shell命令 - 简书](https://www.jianshu.com/p/5d999a668e79) 
2. [Python split() 函数](https://blog.csdn.net/doiido/article/details/43204675) 
3. [python-gitlab](https://python-gitlab.readthedocs.io/en/stable/install.html) 
4. [在线JSON校验格式化工具（Be JSON）](https://www.bejson.com/) 
5. [json-xls.com](http://www.json-xls.com/json2xls) 
6. [erica师姐的笔记](https://gitlab.com/101camp/7py/tasks/-/wikis/IMHO/200330-101camp6py-erica-ch1d1) 



github相关：

1. 这2篇介绍了分支、指针、快照、head等基本概念： [Git - 分支简介](https://git-scm.com/book/zh/v2/Git-分支-分支简介)   [创建与合并分支 - 廖雪峰的官方网站](https://www.liaoxuefeng.com/wiki/896043488029600/900003767775424) 

2. 这里有一个重要概念，本地分支和远程分支，  [Git自学成才——新建远程分支和删除 - 简书](https://www.jianshu.com/p/ea1dab2de419) 
3. 如果出现需要本地与远程关联： [Git远程03：分支的upstream | Learnit](https://higoge.github.io/2015/07/06/git-remote03/) 
4. git命令总结： [Git在工作中的小总结 - 简书](https://www.jianshu.com/p/86ef009e5c86) 
5. Checkout 总结： [【Git】checkout 用法总结 - 简书](https://www.jianshu.com/p/cad4d2ec4da5) ，这里有一个点：**查看工程下面的.git文件夹**



如何获取第一个commit:

1. [git获取第一次commit提交记录 - 满堂大杂烩](https://zxyty.github.io/2018/08/29/git%E8%8E%B7%E5%8F%96%E7%AC%AC%E4%B8%80%E6%AC%A1commit/) ：cli code

2. [Commits — python-gitlab 1.15.0 documentation](https://python-gitlab.readthedocs.io/en/stable/gl_objects/commits.html)  

3. [Python获取gitlab提交历史! - 知乎](https://zhuanlan.zhihu.com/p/66491782) ：python code
4. [Commits API | GitLab](https://docs.gitlab.com/ee/api/commits.html) ，

| Attribute      | Type           | Required | Description                                                  |
| :------------- | :------------- | :------- | :----------------------------------------------------------- |
| `id`           | integer/string | yes      | The ID or [URL-encoded path of the project](https://docs.gitlab.com/ee/api/README.html#namespaced-path-encoding) owned by the authenticated user |
| `ref_name`     | string         | no       | The name of a repository branch, tag or revision range, or if not given the default branch |
| `since`        | string         | no       | Only commits after or on this date will be returned in ISO 8601 format YYYY-MM-DDTHH:MM:SSZ |
| `until`        | string         | no       | Only commits before or on this date will be returned in ISO 8601 format YYYY-MM-DDTHH:MM:SSZ |
| `path`         | string         | no       | The file path                                                |
| `all`          | boolean        | no       | Retrieve every commit from the repository                    |
| `with_stats`   | boolean        | no       | Stats about each commit will be added to the response        |
| `first_parent` | boolean        | no       | Follow only the first parent commit upon seeing a merge commit |
| `order`        | string         | no       | List commits in order. Possible values: `default`, [`topo`](https://git-scm.com/docs/git-log#Documentation/git-log.txt---topo-order). Defaults to `default`, the commits are shown in reverse chronological order. |



其他

1. [Python 列表(List) | 菜鸟教程](https://www.runoob.com/python/python-lists.html)

   [JSON 简介 | 菜鸟教程](https://www.runoob.com/json/json-intro.html)

   [JSON数据 — python3-cookbook 3.0.0 文档](https://python3-cookbook.readthedocs.io/zh_CN/latest/c06/p02_read-write_json_data.html) 

   [Working With JSON Data in Python – Real Python](https://realpython.com/python-json/) 

   [Convert cURL to Python requests](https://curl.trillworks.com/#python) 

   

永远的: [提问的智慧](https://github.com/DebugUself/How-To-Ask-Questions-The-Smart-Way/blob/master/README-zh_CN.md)


## logging:

> 用倒序日期排列来从旧到新记要关键变化


- 200427 SS init.