> 蟒营™学习笔记

    [note] 乐豆派@sunnyseed: CI/CD 像是在读侦探小说 

## 背景

> 额外任务, 若有余力, 也请完成

- 如何通过 CI/CD (持续集成/部署) 功能，令 gitlab 可以感知到小队分支变化
  - 统计数据
  - 数据可视化
  - 团队笔记网站更新发布
- **简单说**, 是否可以将以往人工指令方式完成的一系列行为，变成可以通过某种仓库行为触发自动化过程?


## 进展

> 就好像是侦探小说，开头说这里有个现场：gitlab有变化时，网站被自动更新了，**犯人**怎么做到的？
>
> ⚠️大量术语，扑面而来。。。

#### 1. 怎么知道git-lab出现了变化？

> 查来查去，发现两种方法：

1. 持续集成（CI）：代码变更后，gitlab自动启动一个管道，去完成某些任务
   - 管道，pipeline：多个不同的 *任务(task)*和 *作业(job)*，串联成的一个流程。完成一个自动启动下一个。 
2. Web hook ：在gitlab订阅一个事件，比如发了一个issue之后，webhook会向你指定的服务器，发送事件的提醒

> CI太高冷，只对commit事件有反应，不如Webhook平易近人，先追求Webhook看看，，，

#### 2. Webhook跑起来

>
>webhook入门，要趟过6个坑：

1. 权限？playground有webhook权限，tasks没有，先上playground

2. 设计？就是附加题，**提示**，中的这一句：

   - 但是, 不依赖任何服务,只通过一个长期运行的主机也一样可以完成低配版 CI/CD 响应

3. 主机？就是一个能够接受 webhook post 来的消息的，web应用。查webhook例子时，找了一个叫 flask：

   ```python
   # 请求处理
   from flask import Flask, request
   
   app = Flask(__name__)
   
   # WebHooks 使用 POST 请求
   @app.route('/', methods=['POST'])
   def git_hook():
       # 这就是收到的 json 本体
       receive = request.data.decode('utf-8')
       if request.json['object_kind'] == 'push':
           # 对commits进行处理
   				...
       return 'success'
   
   if __name__ == "__main__":
       app.run(debug=True, port=5050, host='0.0.0.0')
   ```

   > 惊不惊喜？意不意外？😀
   >
   > 这段话是几个帖子凑成的，跑通，2小时过去了。但回头看，真的简单。

4. 外网？怎么让gitlab找到我的电脑？突然想起ch4题目中，提到ch3的坑：

   - 为什么不能将内网应用直接发布到外网?

   我们组因为直接发布在gitlab page上，ch3没有使用自己的服务器。但6py结业bp上，erica组用过一个内网穿透工具，记忆犹新，小本子翻出来，叫做ngrok ~  [ngrok - secure introspectable tunnels to localhost](https://ngrok.com/) 又是一个神奇小子。安装、运行，又是只要一句：

   ```
   ngrok http 5050
   ```

   

5. 组装？配上webhook`http://sunnyseed.ngrok.io/hook`，之后网络结构是下面这样的

   ![](https://tva1.sinaimg.cn/large/007S8ZIlgy1gf4g2tbogoj316y0u0dl8.jpg)

6. 自动？进一步修改 flask 的响应语句，获取三种不同的时间：commit\issue\note，存放在csv中，再用小组此前设计的，pandas语句统计、输出成html，就ok啦。 ～ 8e314cd4d2b8771642fdfec0c0cfce60384b1b59

   ```python
       # comments
       if request.json['object_kind'] == 'note':
           c0 = 'comment'
           c1 = time.strftime("%Y%m%d%H%M%S", time.localtime())
           c2 = request.json['user']['username']
           c3 = "1" #都是一次
           c = f"{c0},{c1},{c2},{c3}"
       
       # 写入csv
       f = open("ci.csv", 'a')
       f.write(f"{c}\n")
       f.close()
       
       # csv -> html
       columns = ['Type', 'Datetime', 'Username', 'Count']
       df = pd.read_csv('ci.csv', names=columns, engine = 'python')
       # group(分组字段)[统计字段1][统计字段2].sum, 之后的部分用于怎加列名
       df_1 = df.groupby(['Type','Username'])['Count'].sum().to_frame('Count').reset_index()
       df_2 = df_1.sort_values(by='Count',ascending=False)
   
       # 保存为 html "网站主目录"
       df_2.to_html('ci.html')
       
   ```

   ![](https://tva1.sinaimg.cn/large/007S8ZIlgy1gf4g2u9aj5j30r60hataw.jpg)

   - 写到这里主体就完成了

#### 3. 使用手册

- 在playground 任意发帖，或者git push之后，访问 [排名展示页](http://101camp.applinzi.com) 就可以实时看到排名变化啦（测试用，历史数据不准确，新增是对的啦🎭，，，应该）

- 服务器的python脚本 -> 8e314cd4d2b8771642fdfec0c0cfce60384b1b59

  


## 问题

> mvp完成，能不能更好

- 问题0 -> 我的mini可是要关机的，怎么7*24小时运行？

- 问题1 -> 如果使用原来的ssg 和 gitlab page，就要使用CI，但又是一堆坑，比如怎么控制git push？

  


## refer
> 过程中参考过的重要文章/图书/模块/代码/...

1.  [什么是持续集成（CI）/持续部署（CD）？ - 知乎](https://zhuanlan.zhihu.com/p/42286143) 
2.  [Gitlab-CI使用教程 - 掘金](https://juejin.im/post/5e1965375188252695366f8b) 
3.  [GitLab CI: Deployment & Environments | GitLab](https://about.gitlab.com/blog/2016/08/26/ci-deployment-and-environments/) 
4.  [Webhooks | GitLab](https://docs.gitlab.com/ee/user/project/integrations/webhooks.html) 
5.  [Python3.6创建虚拟环境并安装Flask框架 - 简书](https://www.jianshu.com/p/0b909759ccb3)
6.  [利用 GitHook 构建持续交付和部署 - CODING 博客](https://blog.coding.net/blog/GitHook-for-delivery-and-deployment) 
7.   [Python Pandas Cannot import QUOTE_MINIMAL - Stack Overflow](https://stackoverflow.com/questions/60912657/python-pandas-cannot-import-quote-minimal) 
8.   [快速入门 — Flask 0.10.1 文档](http://docs.jinkan.org/docs/flask/quickstart.html#quickstart) 
9.   [关于某些时候print函数不会立即打印的解决办法_python_PerryDP的博客-CSDN博客](https://blog.csdn.net/qq_36711420/article/details/79631141) 
10.   [python logging模块使用教程 - 简书](https://www.jianshu.com/p/feb86c06c4f4) 
11.   [关于python：Pandas – 将列名添加到groupby的结果中 | 码农家园](https://www.codenong.com/51163975/) 
12.   [Python 异常处理 | 菜鸟教程](https://www.runoob.com/python/python-exceptions.html) 
13.   [python - Flask Value error view function did not return a response - Stack Overflow](https://stackoverflow.com/questions/25034123/flask-value-error-view-function-did-not-return-a-response) 
14.   [Python3 日期和时间 | 菜鸟教程](https://www.runoob.com/python3/python3-date-time.html) 
15.   [通过Gitlab的WebHooks实现网站内容同步 - 哔哩哔哩](https://www.bilibili.com/read/cv5113831/) 
16.   [Webhooks | GitLab](https://docs.gitlab.com/ee/user/project/integrations/webhooks.html) 
17.   [学习python中的pandas有没有好的教程推荐？ - 知乎](https://www.zhihu.com/question/56310477) 
18.   [Docker 学习新手笔记：从入门到放弃 - Joe’s Blog](https://hijiangtao.github.io/2018/04/17/Docker-in-Action/) 
19.   [Docker 入门教程 - 阮一峰的网络日志](https://www.ruanyifeng.com/blog/2018/02/docker-tutorial.html) 
20.   [使用docker第一步 | Docker中文指南](https://doc.yonyoucloud.com/doc/chinese_docker/articles/basics.html) 
21.  构建gitbook并基于gitlab自动发布. Google



永远的: [提问的智慧](https://github.com/DebugUself/How-To-Ask-Questions-The-Smart-Way/blob/master/README-zh_CN.md)


## logging:
> 用倒序日期排列来从旧到新记要关键变化


- 20050523 SS init.





